let firstNumber = prompt("Enter first number f1");
let secondNumber = prompt("Enter second number f2");
let number = prompt("Enter number n");

function fibo(number, firstNumber, secondNumber) {
  if (number === 2) return secondNumber;
  if (number === 1) return firstNumber;
  let f1 = +firstNumber;
  let f2 = +secondNumber;
  let f3;
  if (number > 0) {
    for (let i = 3; i <= number; i++) {
      f3 = f1 + f2;
      f1 = f2;
      f2 = f3;
    }
  } else if (number < 0) {
    for (let i = -3; i >= number; i--) {
      f3 = f1 - f2;
      f1 = f2;
      f2 = f3;
    }
  }
  return f3;
}

let result = fibo(number, firstNumber, secondNumber);

console.log(result);
